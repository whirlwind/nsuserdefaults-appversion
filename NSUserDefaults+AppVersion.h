//
//  NSUserDefaults+AppVersion.h
//  food
//
//  Created by 詹 迟晶 on 12-5-7.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (AppVersion)
- (NSString *)lastVersion;
- (NSDate *)lastVersionInstallDate;
- (BOOL)lastVersionIsCurrentVersion;
- (NSDate *)lastTerminateTime;
- (int)startupCount;
- (void)increaseStartupCount;
- (void)updateTerminateTime;
@end
