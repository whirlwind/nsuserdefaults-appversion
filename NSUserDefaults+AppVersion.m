//
//  NSUserDefaults+AppVersion.m
//  food
//
//  Created by 詹 迟晶 on 12-5-7.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "NSUserDefaults+AppVersion.h"

@implementation NSUserDefaults (AppVersion)
- (NSInteger)lastBuild{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"last_build"];
}
- (NSString *)lastVersion{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"last_version"];
}

- (NSDate *)lastVersionInstallDate{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"last_version_install_date"];
}

- (BOOL)lastBuildIsCurrentBuild{
    NSNumber *appBuild = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"];
    if (appBuild == nil) {
        return NO;
    }
    NSUInteger build = [appBuild intValue];
    return build == [self lastBuild];
}

- (BOOL)lastVersionIsCurrentVersion{
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"];
    return [appVersion isEqualToString:[self lastVersion]];
}

- (NSDate *)lastTerminateTime {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"last_terminate_time"];
}

- (int)startupCount{
    if ([self lastVersionIsCurrentVersion]) {
        NSNumber *countNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_version_count"];
        if (countNumber == nil) {
            return 0;
        }
        return [countNumber intValue];
    }else {
        return 0;
    }
}

- (void)increaseStartupCount{
    int count = [self startupCount];
    if (![self lastVersionIsCurrentVersion]) {
        [[NSUserDefaults standardUserDefaults] setObject:[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"] forKey:@"last_version"];
          [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"last_version_install_date"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:count + 1] forKey:@"last_version_count"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)updateTerminateTime {
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"last_terminate_time"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
